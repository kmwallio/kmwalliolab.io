---
layout: post
title: 2 Weeks at Microsoft
date: 2012-05-24 20:17
comments: true
categories: life
teaser: /images/ms/dwntwn_seattle.jpg
---

So I've been working at Microsoft for a whole two weeks.  It's shocking how fast time flies because it still feels like I was in school a week ago.

For those of you wondering, I'm currently in the Windows Kernel Platform Group in Building 9.  Even though it's been two weeks, I still get lost in the building.  It's a labyrinth.

It still feels surreal being at Microsoft.  It's started sinking in, but at the same time, I'm waiting for someone to walk through the door and say "Just kidding!  We got you good."

<!-- more -->

I don't think I'm a poor example of someone to hire; I just never pictured myself at Microsoft until they flew me out for the interview.  The original image I had of Microsoft was misguided, and now I think they're *kind of* cool.  I'm definitely enjoying being here more than I thought I would.  I thought working on the "Kernel Team" was going to be painful and daunting, but they have me working on a project I find interesting.

What project am I working on?  I can't tell you.

## Yo Dog, so I heard you like Windowz?

It's been a long time since I've actually programmed on Windows for Windows.  I was really shocked to find that I enjoy it.  Not enjoy it like I'm throwing away my Apple products, but I enjoy it as in I now see it's appeal.

I mean, I just spent money on [Coda 2](http://panic.com/coda) and [Diet Coda](http://panic.com/dietcoda).  So don't worry people, I haven't transformed or anything like that.  I still like playing with Perl and Python.  Although... I guess I could do that on Windows... But, I'd need to find some awesome tools for that on Windows that I'd enjoy using and make me justify making the switch.  Plus, I'd have to find a laptop (or tablet?) that I approve of.  But I do have [Windows 8](http://windows.microsoft.com/en-US/windows-8/consumer-preview) on my macBook, and it does run beautifully.

I'm hoping to do some stuff in Perl this weekend, so yeah... I'm still me I think.

## Football! (or as we Americans say, "Soccer")

I saw a soccer game this week.  [Sounders](http://soundersfc.com/) vs. [The Crew](http://thecrew.com/).  The Sounders were set to win... or at least that's what everyone told me.

It was a really fun experience.  I'm not a major soccer fan or anything, but the stadium was in Downtown Seattle, and you could see the whole city.  The game was interesting because everyone said the Sounders would win without any effort, and then they lost 0 to 2.

It was kind of an upset, but at the same time, it made the game more interesting.  I'm still confused about soccer rules though, and the whole off-sides thing.

## Closing remarks...

I'll update this post with some pictures and things later.

I've been having a lot of fun up here, and there's a lot of interesting things to see.  Everyday is an adventure, and I've been loving every moment of it.
