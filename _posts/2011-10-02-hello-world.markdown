---
layout: post
title: "Hello World"
date: 2011-10-02 21:09
comments: true
categories: 
---

This is my first post using [Octopress](http://octopress.org/).  I'm just trying it out to see how things go with it.

I hope to have some awesome content up here shortly.  Everything here is pretty much just a learning experience for me that I hope others will find useful as well.
