---
layout: post
title: On Fear of Open Source
date: 2020-08-29 22:30
categories: [projects, vala, markdown]
teaser: /images/thiefmd/thiefmd.png
comments: true
---

It might sound dumb, but open source terrifies me. I love reading about people suffering from [impostor syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome). I'm also pretty sure that's why I write a blog post about once every 2 years.

With open source, I have no clue how the licenses work... I created a fork of an open source markdown editor, and took parts of other open source code to make it work how I want. School taught me how to put software together, but I don't think it ever taught me how to deal with licenses and open source.

![](/images/thiefmd/live_preview.gif)

The original license is GPLv3. So most of that is okay, but for the preview, I am using [MIT Licensed CSS](https://github.com/markdowncss/splendor). So while I try to figure out open source licensing and deal with my [solitary confinement](https://www.seattletimes.com/seattle-news/politics/gov-inslee-extends-washington-states-coronavirus-stay-home-order-through-end-of-may-4/), let me tell you the story of the Markdown editor I wanted to keep just for myself.

<!-- more -->

### ThiefMD

In my [last post](/blog/2018/06/10/back-to-xubuntu), I said I was using [Quilter](https://github.com/lainsce/quilter). I still follow the development and try to identify areas I can contribute. Some issues I ran into was apps on [elementary OS](https://elementary.io/) render differently that on Ubuntu, and something about fear of a pull request being rejected.

I also wanted something closer to [Ulysses](https://ulysses.app) from macOS. There are a bunch of Electron Markdown applications, but they don't feel or act native.

I started calling my project ThiefMD, since I'd just be stealing code and concepts from other programs. Whenever I go back to macOS or Windows, it's because I'm missing some utility that satisfies both the functionality I need along with the emotional response.

With open source, I can work on making the software I use be both functional and elicit the emotional response I'm looking for, but I also have no idea if I'm possibly doing something illegal.

![](/images/thiefmd/panel_animation.gif)

Opening up the code to everyone also increases my fear of scrutiny. I started working on Quilter and ThiefMD to learn Vala. The library is a [TreeView](https://valadoc.org/gtk+-3.0/Gtk.TreeView.html) and the sheets are [Buttons](https://valadoc.org/gtk+-3.0/Gtk.Button.html) with some CSS. As I look more and more at the code, I feel embarrassed about some decisions younger-me made.

![](/images/thiefmd/sidebar.png)

While I like to think I'll use this tool to write some great novel, I normally use it just to organize my [Jekyll](https://jekyllrb.com/) blog.

### Downloading

ThiefMD has actually reached the phase we're I'm happy to use it. There's a lot more I want to do, but I've reached a point where I'm proud of what I accomplished so far.

[You can check out the releases on GitHub](https://github.com/kmwallio/ThiefMD/releases).

#### Notes

* Wallpaper by [Matt Hoffman](https://unsplash.com/@__matthoffman__) on [Unsplash](https://unsplash.com/photos/kmGgAs77k-g)
* GTK Theme is [Vimix Dark Beryl](https://github.com/vinceliuice/vimix-gtk-themes) with [Vimix Icons](https://github.com/vinceliuice/vimix-icon-theme)