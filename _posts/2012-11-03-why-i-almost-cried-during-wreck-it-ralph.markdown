---
layout: post
title: "Why I almost cried during Wreck It Ralph, but not Wall-E"
date: 2012-11-03 20:54
comments: true
categories: [life, movies]
teaser: /images/wreck/wreck-it.jpg
---

**Caution: Spoilers may be present**

[Wreck it Ralph](http://disney.go.com/wreck-it-ralph/) has to be one of my favorite movies of 2012 so far.  Yes, it is a movie targeted at children (I believe?), but it shockingly pulled on some of my heart strings and almost made me cry.

No main characters died, no character was close to death, and no character suffered any major loss (except for back story things).  The main issue--and it's a big one--is that Disney hinted at a possible "Happy Ending" that didn't have a wish come true feeling in the end.

<!-- more -->

## Disney Movies Need a Happy Ending

The moral of the story (hinted at from the beginning) is "you need to accept who you are."  Labels mean nothing.

In the movie, Ralph is discontent with who he is.  He is a "bad guy" who isn't appreciated by the other characters in his game.  He winds up making a bet where if he can come back with a medal, he'll get to live in the penthouse with the other members of his game.

This leads him on an adventure where he winds up in the [Sugar Rush](http://www.youtube.com/watch?v=-LEY2rO5Sl4) arcade game.

Ralph meets a misfit named Vanellope von Schweetz.  Vanellope is a "glitch" who doesn't fit into her game.  The way for her to become a part of the game is to be a real racer.

She and Ralph build a real race car together, and they have a touching moment and some bonding (or something like that).  It looks like she will have a chance to race and become a part of the game.

However, that's where the drama comes in.  Since she is a glitch, she cannot leave the game and visit game central--she is trapped in her game.  King Candy goes and informs Ralph that if Vanellope participates in the race and wins, she will be a playable character.  If she is playable, then the players might not like her glitch and think the game is broken.  The game could become unplugged, and Vanellope would be trapped in the game and trashed along with it.

In order to protect Vanellope, Ralph destroys the car he and Vanellope built in order to protect her from winning.  This is where I almost cried.

As emphasized from the beginning of the movie, the moral is all about self acceptance.  Vanellope is a glitch, and she can't race for her own protection.  That is the idea introduced.

The scene itself might not be a tear jerker, but it's the thought or idea that came into my head which almost brought about the tears.

Disney is all about dreams and wishes coming true.  The characters were wishing for acceptance, and they had each other.

My tears almost came out of the concern that Disney may have made a movie where Ralph and Vanellope would find acceptance, but not get their goals (or other dreams) fulfilled.  Their struggle throughout the movie would have led to another solution, one not on their path, one that is not the happy ending they were searching for.  It would have still provided the same morals and values, but not a truly happy ending.

I don't think any other Disney movies have suggested the possibility of a happy ending that the character didn't yearn for.  Ralph and Vanellope yearned for acceptance, and they sought that through medals and racing.  For a split second, the plausibility that they could find acceptance some other way was terrifying.

In real life, we can find happiness (not always as planned), but in Disney movies, Happy Endings always correspond with dreams come true.  To suggest in a plausible and acceptable way that the lead characters would get what they want without fulfilling their dreams--that's where my tears almost came from.

But, Disney movies always have a happy ending where dreams come true, and that's why I only *almost* cried during 'Wreck it Ralph', but not 'Wall-e'.

On another note, the short before the movie was awesome and well worth the ticket price alone.
