---
layout: post
title: "Drinking the Microsoft Kool-aid"
date: 2012-02-08 19:27
comments: true
categories: [life, news]
teaser: /images/ms/ms_bwn.jpg
---

Last Friday, I interviewed at Microsoft for an internship.  I started at 9.30 am and went till roughly 4.00 pm.  It was a long and painful process, but fun, educational, and worth it none-the-less.

In all honesty, the whole weekend was a whirlwind of emotions.  I'm a bit of a Mac fanboy at heart.  **I don't hate Windows**; it's just that Mac OS X provides an environment I like by default, where Windows requires me to setup things (like [StrawBerry Perl](http://strawberryperl.com/), [Python](http://python.org/), and etc or install [Cygwin](http://www.cygwin.com/)).  So the moment I was caught off-guard--and it showed on my face--was when one of the interviewers asked if I owned a PC.

I interviewed with four people (whom I'd like to thank very much for providing me with such an awesome opportunity).  It was a roller coaster of emotion.

Some of my friends who have an interview with Microsoft coming up ask me for advice and answers to the questions the interviewers ask.  I guess my main advice would be honest about who you are and what you know, don't forget to think, and don't forget to breath.  As for the answers to the questions, I think it's better if you don't know the answers before hand because it might not be the answer they're looking for.

<!-- more -->

My first interview was a nice way to start.  The interviewer was friendly and the questions he asked weren't too hard.  The main item covered was tree traversal, and he asked some other core concepts.  I'm pretty sure that I said "I'm nervous" and "I really want this internship" about 15 times during each interview.

My second interview was difficult, but ended on a high note.  I really think that Microsoft enjoys torturing the interviewees.  You're nervous and just want to escape from the pressure, but while you're under that pressure, Microsoft forces you to have a lunch interview.  It was really difficult for me to eat, but I really enjoyed conversation.  We talked about Microsoft, how it was to work there, and about the eco-friendly silverware and whatnot.  The programming question was much harder than the first, but really interesting.  It was really similar to the problems that have on [TJU Online Judge](http://acm.tju.edu.cn/toj/), so that'd be a good place to prepare for the programming questions.  I managed to give an acceptable solution, and moved on with confidence.

Now the third interview made me want to go home.  I have nothing against the interviewer, but my brain was starting to turn to mush.  I was asked Operating System concepts questions (I was interviewing with the Windows Core team).  Operating System concepts are probably my weakest area of knowledge in Computer Science.  I feel like I completely bombed the question, but I made sure to explain everything I did and the logic behind it.  Needless to say, I felt broken after the interview and wanted to go.

My last interview was pleasant enough.  I was concerned that this was the "You made it far, but not far enough, thanks for trying" interviewer.  The questions were more about my thoughts and feelings on things, and not too technical.  The last question was similar to the first, and I made sure not to make the same mistakes I did earlier.  During my writing of the code, I asked "Can the tree have cycles?" and he replied "Not yet".  That got a good reaction out of me, but it provided me with time to think about the solution if cycles were present.

After the interview, they made me wait back in the HR building.  At the start of the day, the HR room was awesome, it had games and showed some of the perks of Microsoft.  At the end of the day, it was everything I'd be missing out on.

My recruiter brought me into a back room, and started talking to me about interesting things in Redmond and Seattle.  I figured this was the "See it while your here" message coming from Microsoft.  She then told me she knew the results of my interview and wanted to know if I wanted to know the results.  I told her I didn't think I could handle the news, but to tell me anyway.

She told me I did well and that they wanted to give me an offer (and she handed me a "My kid has a cooler job than yours" bumper sticker).  I was ecstatic, but I needed to ask her to repeat it several times before I believed her.  I don't remember much of the rest of the conversation other than me saying some stupid things and asking "Is it alright to call my mom, it's safe to tell her I'll be interning here?"

So it was a pretty crazy experience, one that I'm proud to have experienced.  So this summer, I'll be in Redmond working on some part of Windows.  Wish me luck?
