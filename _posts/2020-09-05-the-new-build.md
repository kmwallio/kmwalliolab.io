---
layout: post
title: My new computer internals
date: 2020-09-09 21:41
comments: true
categories: [life, tools]
teaser: /images/pc-build/ryzen-inside.jpeg
---

A few years ago, I switched to a [Dell XPS 13](https://www.dell.com/en-us/shop/dell-laptops/sr/laptops/xps-laptops/13-inch) as my laptop. At the time, it was more powerful than my desktop. This wasn't an issue, but now that I'm working from home, my desktop is feeling a little week.

I upgraded my 2014 desktop this weekend from an Intel i5 to an [AMD Ryzen 9 3900XT](https://www.amd.com/en/products/cpu/amd-ryzen-9-3900xt). What was supposed to be "just a processor and motherboard" upgrade turned into so much more...

<!-- more -->

Originally, I was planning on sticking with Intel. With the current state of the world, it's hard to find an i9 at MSRP price in stock. Ryzen seemed readily available, and readily able to appease my upgrade needs.

![](/images/pc-build/msi-trident.jpg)

I wound up going for:

* [AMD Ryzen 9 3900XT](https://www.amd.com/en/products/cpu/amd-ryzen-9-3900xt)
* [MSI MEG x570 Ace](https://www.msi.com/Motherboard/MEG-X570-ACE)
* [G.Skill Trident Z Neo](https://www.gskill.com/products/1/165/326/Trident-Z-Neo) - 64GB
* [Samsung 970 EVO NVMe M.2 SSD](https://www.samsung.com/us/computing/memory-storage/solid-state-drives/ssd-970-evo-nvme-m-2-1tb-mz-v7e1t0bw) - 2x1TB

I kept my current [MSI GAMING GeForce GTX 1060 6G](https://us.msi.com/Graphics-card/geforce-gtx-1060-gaming-x-6g.html).

I went with the Trident Z Neo because reviews for other RAM said it didn't work with AMD when running at higher speeds.

The Samsung 970 EVO is a PCIe 3.0 M.2 SSD. The MEG x570 Ace supports PCIe 4.0 devices, but I've used Samsung SSD's before, and they've been more reliable than other brands I've tried, so I stuck with them.  I got 1 TB for [Ubuntu](https://ubuntu.com), and 1 TB for [Windows 10](https://www.microsoft.com/en-us/windows).

I originally didn't want to buy a new case, but the old setup seemed like it'd be good to turn into a game machine hooked up to my TV, especially now that [Persona 4 Golden](https://store.steampowered.com/app/1113000/Persona_4_Golden) is available on Steam.

Since I wanted something that could run Steam hooked up to my TV, I decided to get a case to house the new PC.

* [Fractal Design Meshify C](https://www.fractal-design.com/products/cases/meshify/meshify-c-tempered-glass/white)
* [Noctua NH-D15 chromax.black](https://noctua.at/en/nh-d15-chromax-black)
* [Noctua NF-A14 PWM chromax.black.swap](https://noctua.at/en/nf-a14-pwm-chromax-black-swap) x4
* [Noctua NF-S12A PWM chromax.black.swap](https://noctua.at/en/nf-s12a-pwm-chromax-black-swap)
* [GAMEMAX RGB850 Rainbow Power Supply](https://www.gamemaxpc.com/productkkk/showproduct.php?id=940)

I actually didn't want LEDs or RGB in the build. My tower hides behind a desk. The non-RGB mother boards and RAM were sold out, as were the non-RGB power supplies. The Fractal Design Meshify C has a big glass side, so maybe I'll try positioning it somewhere I can see into the box?

## And the results...

The most shocking thing to me is how much fast [VS Code](https://code.visualstudio.com) search is. The results are near instant for my personal projects.

Compilation for my projects has also decreased. It feels like any lag is because of printing to the console.

Even though a PC is a PC, and I'm still running [Ubuntu](https://ubuntu.com), I've been programming more and enjoying working on side projects since the build 😜

![](/images/thiefmd/thief-themed.png)

I stayed up too late adding [Ulysses Theme](https://styles.ulysses.app/themes/) support to [ThiefMD](https://thiefmd.com). I'll write more on that later. The theme above is [Tomorrow Dark](https://styles.ulysses.app/themes/tomorrow-qyp).