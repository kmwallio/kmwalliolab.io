---
layout: post
title: Well It Works On My Machine
date: 2020-10-10 21:39
comments: true
categories: [projects, vala]
teaser: /images/on-my-machine/teaser.png
---

One of the things I always forget about is accessibility and defaults. After advertising my open source project, I was reminded of these things. People sent screenshots of [ThiefMD](https://thiefmd.com) with unusable header bars and white on white menus and dialogs. The [Gtk Theme I use](https://vinceliuice.github.io/theme-vimix.html) made ThiefMD look super cool on my machine. Users had a different experience.

<!-- more -->

I was super ecstatic to see [GTK3 supports CSS Styling](https://developer.gnome.org/gtk3/stable/chap-css-overview.html). The last time I used Linux, app customization required effort, and the app would look out of place. CSS Styling would let me keep applications feeling native and at home, while still offering a different experience.

This power comes with responsibility. [Hundreds of GTK Themes](https://gnome-look.org) exist. Your users probably aren't using your theme, and [tweaking the look](https://www.reddit.com/r/unixporn/) is a big part of the experience.

The out of box experience determines if the user keeps or deletes your app. The first issue I didn't create showed how broken ThiefMD was.

![](/images/on-my-machine/the-issue.png)

Depending on your eyesight and monitor, it might look hip and cool. The contrast ratio was [1.18](https://contrast-ratio.com/#%23DDDDDD-on-%23EFEFEF). Not easy to use or understand.

I quickly disabled the feature and published a new version that defaults to the User's GTK Theme.  Looking at the app, the only visible action item is closing it 😦️.

Theme options are still a big part of ThiefMD, they're just disabled by default.

There's a lot for me to learn in application development. I haven't even gone too deep into accessibility. Here are some initial steps I'm hoping to take.

### Contrast Checking

I installed [Contrast](https://flathub.org/apps/details/org.gnome.design.Contrast) to check default colors. Monitors, eyesight, glasses, and so much more can alter your application's appearance. Validating contrast won't help it look better, but it will help maintain usability.

### Developing with the Defaults

I've switch my theme to [Adwaita](https://gitlab.gnome.org/GNOME/gtk/tree/master/gtk/theme/Adwaita). This is the default GNOME experience for so many users. [flatpak](https://flatpak.org) applications also [default to Adwaita](https://docs.flatpak.org/en/latest/desktop-integration.html#applying-themes).

![](/images/on-my-machine/defaults-no-so-bad.png)

Switching to this has let me catch issues related to my CSS. Even if the CSS I add doesn't set some field, the user's GTK Theme may. This can create random lines or padding issues. I've started adding fixes to the Dynamic CSS, so if you find any issues, [send a pull request or issue](https://github.com/kmwallio/ThiefMD).

### Checking the Defaults

I've installed a [Fedora](https://getfedora.org/) Virtual Machine. This allows be to package for both DEB and RPM.

This also allows me to have a clean room. I can make sure the [defaults are ideal](https://www.theguardian.com/technology/2013/dec/01/default-settings-change-phones-computers). As much as I want the user to "*discover*" features inside the application, they can't if it's not usable from the start.

[writegood](https://github.com/ThiefMD/libwritegood-vala), a writing suggestion feature in ThiefMD is disable by default. writegood also doesn't persist. It changes text color along with scanning the users file over and over. The feature is useful, but as a default experience or persistent experience, it's intrusive.

### Give Options to the User

This is more of a plug than anything. I've launched a [theme site for ThiefMD](https://themes.thiefmd.com). I was thinking of having a **Dark Mode** and **Light Mode** for ThiefMD but decided I like Themes way too much.

![](/images/on-my-machine/thiefmd-movingforward.png)

I know ThiefMD won't be the application for everyone, but I'm working hard to make sure it's not an application for just me.

If you have tips and tricks, feel free to share them!
