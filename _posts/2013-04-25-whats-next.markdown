---
layout: post
title: "What's Next?"
date: 2013-04-25 20:46
comments: true
categories: [life]
teaser: /images/what_next/grad_reg.jpg
---

So I finally did it.  I defended my thesis, and now I'm waiting for commencement and receiving my diploma.  It's hard for me to believe that I've spent the past 6 years at university.  It just seemed to fly by.

One of the questions that no one seems to be asking me is "What's next?"  I understand because everyone knows I'm going off into the real world to work for Microsoft.  I don't want to say that's not really "What's next?"; it's just that there's so much more that can go on than just that.  And because of that belief, I feel like I've reached my [quarter-life crisis](http://en.wikipedia.org/wiki/Quarter-life_crisis).

<!-- more -->

For the past some-odd years, I've been going to school.  School doesn't define what I do or who I am though, right?  I feel like the same thing can be said about people's occupations or jobs.  They spend eight hours at work, and then outside of work they're a whole different person.  For grad students, that's some what a different story because our work travels home with us.  As an undergrad, you're a student by day, and at night you're an [actor](http://cp.fit.edu), [developer](http://acm.fit.edu), [journalist](http://crimson.fit.edu/), or whatever else you wanted to be.  Grad students are students by day and researchers by night.

As I head off to Microsoft, I know I'll be a software developer by day, but I'm not too sure what my free-time will entail.  I hope research stays a part of it, but I'm hoping for more than that.  I could join a band, become an actor, dabble in journalism, become a photographer, bank robber, part-time artist, masked vigilante, or anything really.  The possibilities seem endless.

One of the things I'm hoping to do is creative writing.  I'm not sure what that will entail quite yet, but I really want to learn how to tell a great story.  It's crazy how people can create new worlds and engage and connect with readers in deep impactful ways.  Look at the people who read *Harry Potter*, *The Hunger Games*, <del>*Twilight*</del>, *The Lord of the Rings*, and some other books[^fn].  I want to be able to write in such a way that transports readers and keeps them coming back for more.

[^fn]: Not *Fifty Shades of Grey*

I think one of my main fears is my job becoming my life.  I don't want my work to define who I am completely.  I want to have something else outside of work that I can be proud of and share with people.

I'm sure I'll find balance.
