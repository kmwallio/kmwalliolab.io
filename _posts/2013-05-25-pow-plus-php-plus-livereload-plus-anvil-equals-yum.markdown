---
layout: post
title: "Pow + PHP + LiveReload + Anvil = Yum"
date: 2013-05-25 19:51
comments: true
categories: [tools, tutorial, php]
teaser: /images/dev-setup/catcher.png
---

I haven't been dabbling in web development lately, and I've missed a lot of things.  I was really excited to see some of the newer tools available out there and put them to use.  Sadly, some of the information out there is old or is project specific.

Because I just got it figured out, I figured I'd write this note on getting it setup to benefit myself and others who may have the same question I did.

Can [Pow](http://pow.cx) work with [PHP](http://php.net) and [Anvil](http://anvilformac.com)?  How can I tie in [LiveReload](http://livereload.com) or [something](https://incident57.com/codekit) [similar](http://fireapp.handlino.com)[^fn-theres-more]?

[^fn-theres-more]: If you're only using Sass or LESS, theres [Scout](http://mhs.github.io/scout-app/) and [SimpLESS](http://wearekiss.com/simpless).  They're free but lack reloading or live previewing.  Some editors support live previewing and will work with Scout and SimpLESS with a little effort (like double saving).

The answer is yes and easily!  So let's get started.

<!-- more -->

## Getting Started

If you noticed, most of the applications mentioned are for Mac[^fn-sorrywindows].  There are similar workflows for Windows.

[^fn-sorrywindows]: There are some similar applications for Windows like [Prepros](http://alphapixels.com/prepros/)

So first, a little introduction to the tools.

### [Pow](http://pow.cx)

In most cases, PHP developers on Mac are encouraged to install something like [MAMP](http://www.mamp.info/en/index.html), [XAMPP](http://www.apachefriends.org/en/xampp.html) or some local installation of [Apache](http://httpd.apache.org).  This is okay, but it requires a lot of configuration for each project on your machine.

The developers at [37 Signals](http://37signals.com) had a similar problem with their [Ruby](http://www.ruby-lang.org/en/) applications and created Pow, "a zero-config [Rack](http://rack.github.io) server for Mac OS X".  Rack is a web server interface.  Rack takes a config file for how to start up and run the server.

[PHP](http://php.net) has a [built-in web server](http://php.net/manual/en/features.commandline.webserver.php) now, but it's not too useful for managing and viewing multiple projects.  Pow is crazy magical in what it does for ruby developers.  Each project can have a local URL like `project.dev`, and luckily, we can harness that power as PHP developers.

### [Anvil](http://anvilformac.com)

When using Apache, we can configure virtual hosts easier(ly) using [VirtualHostX](http://clickontyler.com/virtualhostx/).  Pow let's you simply create a link to the directory.  Even easier is [Anvil](http://anvilformac.com).

The crazy awesome tool sits in your menu bar and lets you control Pow and create sites for your projects.  You just click on the `+`, browse to your projects folder, and type in the URL you want to access it at.  This is loads better than running and configuring Apache in my opinion.

### [LiveReload](http://livereload.com)

There are so many new tools and languages now to "make things easier".  There's [LESS](http://lesscss.org), [Sass](http://sass-lang.com), [Jade](http://jade-lang.com), [HAML](http://haml.info), [Coffee-Script](http://coffeescript.org), and so much more.  Managing compilers and remembering to update files can be hard to do.  That's where LiveReload comes into play.  When you save your less, Jade or coffee script file, LiveReload automatically compiles them to CSS, HTML, or JavaScript.

It also takes editing files to another level.  It will refresh your browser(s) when you save your files.  If you're editing CSS related files, it will only reload the style sheet (in case you're working on dynamic pages).  It's pretty cool.  To get the most out of it, you have to insert some JavaScript into your code or install browser extensions.  If that sounds unpleasant to you, I head [CodeKit](http://incident57.com/codekit) is a good alternative[^fn-local-only].

[^fn-local-only]: One of the main reasons I use LiveReload instead of CodeKit is LiveReload can work with multiple devices.  Using [xip.io](http://xip.io) with Pow, I can use my iPad and Windows PC to preview my work whenever I hit save.  If you're working on a single device, I think CodeKit is slightly better in terms of communicating error and what's going on.

## Setting things up

First, install [mac homebrew](http://mxcl.github.io/homebrew/).  This is a package manager for Mac.  It's a lot smoother than Fink or MacPorts, and it can get us up and running quicker.

Using Homebrew, install `node` and possibly `ruby` (we want version 1.9.3 or later).

Now we can install Pow.  Just run `curl get.pow.cx | sh`.

If you use [Octopress](http://octopress.org), you can use [Anvil](http://anvilformac.com) to create a local `.dev` domain for your blog.  Cool, right?

So rack is for ruby, and luckily, there's some "legacy" support for rack through the [rack-legacy](https://github.com/eric1234/rack-legacy/wiki) gem.  This calls the `php-cgi` whenever a php script is accessed through rack.  So let's install `rack-legacy`

```
gem install rack-legacy rack-rewrite
```

One problem, Mac OS X doesn't come with the `php-cgi` by default; we have to compile this ourselves.  But don't fret, we can use home-brew thanks to [homebrew-php](https://github.com/josegonzalez/homebrew-php).  Simply "tap" into homebrew-php

```
brew tap homebrew/dupes
brew tap josegonzalez/homebrew-php
```

Look at `brew options php54` to see if there's anything extra you want.  Minimally, we need `--with-cgi`.

```
brew install php54 --with-cgi
```

So now we have PHP 5.4 with php-cgi installed.  We can now setup our PHP site using Pow!  All we have to do is create our config.ru

### No Rewriting

If you're not using mod_rewrite, we can easily create a config.ru

``` ruby
require 'rack'
require 'rack-legacy'
require 'rack-rewrite'

INDEXES = ['index.html','index.php', 'index.cgi']

ENV['SERVER_PROTOCOL'] = "HTTP/1.1"

use Rack::Rewrite do
  rewrite %r{(.*/$)}, lambda {|match, rack_env|
    INDEXES.each do |index|
      if File.exists?(File.join(Dir.getwd, rack_env['PATH_INFO'], index))
        return rack_env['PATH_INFO'] + index
      end
    end
    rack_env['PATH_INFO']
  }
end

use Rack::Legacy::Php, Dir.getwd, '/usr/local/bin/php-cgi'
use Rack::Legacy::Cgi, Dir.getwd
run Rack::File.new Dir.getwd
```

With this, PHP scripts are executed, and we specify `['index.html','index.php', 'index.cgi']` as index files.

### With Rewriting

I haven't played with it too much, but [this gist](https://gist.github.com/brianpattison/4129846) seems to target Wordpress and CakePHP.

## Working with Live Reload

[<img style="float: right;" src="/images/dev-setup/find_ip.png" width="150" />](/images/dev-setup/find_ip.png)LiveReload will give you a script tag to insert into your code.  We just need to make one small modification.  We want to change the `` to point to our machine directly.  We can use [xip.io](http://xip.io) for this, and our code becomes

<div class="clearer"></div>

``` html
<script>document.write('<script src="http://LOCAL_IP.xip.io:35729/livereload.js?snipver=1"></' + 'script>')</script>
```

where `LOCAL_IP` is your [local IP address](/images/dev-setup/find_ip.png).

### Preview from other devices

So let's say you created `php-project.dev`.  To access it from another device, you can navigate to `php-project.LOCAL_IP.xip.io`, and you'll see the site on that device!  If it's something like an iPad, Tablet, or Laptop for previewing and you have the LiveReload code on the page, it will refresh whenever you save.  How cool is that?  Edit on your Mac, see the results as you save on your Tablet and Windows Device (or from a [Virtual Machine](https://virtualbox.org)).

### Other Notes

#### Working with [Coda](http://panic.com/coda)

[<img src="/images/dev-setup/coda2.png" width="250" style="float:right" />](/images/dev-setup/coda2.png)If you're just using plain old CSS, PHP, JavaScript, and HTML, then you don't need LiveReload and the setup for Pow will work for your `Local URL`.  Coda will reload the preview on save.  [Air Preview](http://www.panic.com/dietcoda) works as well.

#### Working with [Espresso](http://macrabbit.com/espresso)

Use [LiveReload](http://livereload.com).  Espresso lacks knowledge about servers and URLs.  It works great for static sites, but if you're doing anything dynamic, Espresso only works great for modifying CSS.

#### Working with [Brackets](http://brackets.io)

I just found out about [Brackets](http://brackets.io).  It seems awesome.  If you're using just plain old CSS, PHP, JavaScript, and HTML, then you don't need LiveReload and the setup for Pow will work for your `Local URL`.  If you're using preprocessors, LiveReload or CodeKit works wonders, but you don't get the awesome HTML+CSS features of Brackets.  The downfall is Brackets' preview only works with Chrome.

#### Working with Others

With this setup, you can use your [favorite text editor](http://tarantsov.com/blog/2012/02/sublime-text-workflow-that-beats-coda-and-espresso/) to edit your web sites.  Any text editor will do.

#### Speed

A lot of people mention that this setup is slow.  I think it works well for development.  Once you get PHP and Pow setup, creating new projects is much easier than adding hosts to Apache.  I can just copy and paste my `config.ru` and use [Anvil](http://anvilformac.com) to get a development site up in seconds.  I wouldn't think of using this for a stand alone deployed PHP site.

For local development, I don't notice and speed problems.  The added simplicity makes me want to ignore what people say about running PHP sites using Rack (at least locally).

I hope this helps others who had the same questions I did.

Happy Coding!

### Works Cited?

* [Legacy Development with Pow](http://stuff-things.net/2011/05/16/legacy-development-with-pow)
* [Pow with Laravel](http://joshbetz.com/2012/11/pow-with-laravel)
* [Running PHP apps alongside Rails apps in Pow (pow.cx)](https://coderwall.com/p/splmua)
