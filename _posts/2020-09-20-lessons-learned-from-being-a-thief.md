---
layout: post
title: Lessons Learned From Being a Thief
date: 2020-09-19 14:59
teaser: /images/thiefmd/theme_themes.jpg
comments: true
categories: [projects, vala]
---

Not too much time has passed, but with staying inside due to COVID-19 and the Washington Wildfires, I've been making a lot of progress on my [markdown editor app](https://thiefmd.com). This week I added Theme Support, Library Organization, Library Export, and possibly some other stuff.

I initially ~~stole~~ forked [a project called Quilter](https://github.com/lainsce/quilter). Quilter's user experience is great, and I would compare it to [iA Writer](https://ia.net/writer) on macOS. I realized one of the applications I missed the most was [Ulysses](https://ulysses.app), and wanted something more like that.

Switching from macOS to [Ubuntu](https://ubuntu.com) has mostly been okay. My main reason for leaving was the laptop keyboard design 😝. As a software developer, a lot of applications I use are already cross-platform. Applications for "creatives" seems lacking in the Linux world. [elementary OS](https://elementary.io/) is fixing that, but I like [feature bloat](https://elementary.io/docs/human-interface-guidelines#avoid-feature-bloat).

I initially sent pull requests for my code changes into Quilter, but I really wanted to do some changes that would take the app in another direction. Thus, ThiefMD was born...

But here's a brain dump of thoughts on how we got here...

<!-- more -->

## It's important to ask questions

While open source and pull requests are cool, they're also somewhat nerve-wracking. There's pages that document [Linus Torvalds](https://adtmag.com/blogs/dev-watch/2014/04/linus-torvalds-rants.aspx) rants about and towards the open source community and its contributors.

All of my interactions with [Lains](https://github.com/lainsce) (the Quilter developer) have been pleasant, I didn't know how to communicate with him or others too well. I probably should have asked him about a road map or vision, but I didn't.

When I reach out to the Ulysses team about releasing a script for converting `ultheme` to `gtksourceview style schemes`, they said:

> Hi Miles,
> 
> Thank you for reaching out.
> 
> Sure, you can release the script. 
> 
> All the best

Future me has to remember to communicate more and to not be afraid of asking questions.

## Sometimes it's better to just write

I had a bunch of stuff planned for [ThiefMD](https://thiefmd.com) a long while back. I just wanted to make sure I did it right.

With open source, people can see the source code. I think I've matured a bit since the initial fork, because now I see reviews as an opportunity to grow. If there's a right or better way to do something, no one will tell me unless they know what I'm trying to do.  This can either be through me having a feature road map, or publishing the code.

After writing code that looks like a gnarly implementation of something, I get even more ecstatic when I see it works. If someone tells me there's a better way to do it, at least my way will be persisted in the history and I'll have some satisfaction on doing it my own way 🤣.

## Don't Repeat Yourself

[DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself) is something hard for me to do on personal projects. Developing software as a day job has meetings and planning. Once I log out of my work computer, I like experimenting and building things like a kid without the Lego instructions.

However, if you look at my drag and drop implementation, there's a lot of copy and pasting along with code that could be simplified. I started abstracting other parts of the code, but a mildly thought out plan could have helped a little.

## Vala's pretty cool

When starting development on Linux, I was looking at [electron](https://www.electronjs.org/) and [flutter desktop](https://flutter.dev/desktop). Part of what I liked about the macOS ecosystem is the apps felt like they belonged on the OS. Experiences were integrated end to end. Electron apps feel like they don't belong, and Flutter doesn't feel integrated.

Vala makes programming [Gtk](https://www.gtk.org) applications that integrate with [GNOME](https://www.gnome.org) easy. I was originally considering [Perl](https://www.perl.org), but compiling to native code was a major benefit in my opinion.

What made me choose Quilter over the plethora of electron markdown editors was the emotional response I had from it and how it felt like part of the system.

Switching from C#/Java to Vala was also pretty easy. [Valadoc](https://valadoc.org) was also a huge benefit in trying to learn and do things.

I'll admit, if you're looking for a language to learn for advancing your career, Vala isn't it. It's definitely fun if you want to develop Gtk applications on Linux.

## Vala's kind of weird

There's a few weird gotcha's in Vala. Most of the issues I hit exist in other languages as well. There's [unowned](https://wiki.gnome.org/Projects/Vala/ReferenceHandling#Unowned_References), [weak](https://wiki.gnome.org/Projects/Vala/ReferenceHandling), and [strong/owned](https://wiki.gnome.org/Projects/Vala/Ownership) references. It's not always clear on when some component will release a reference. I was chasing a bug where I thought I release all references to a [TextView](https://valadoc.org/gtk+-3.0/Gtk.TextView.html), but [GtkSpell](http://gtkspell.sourceforge.net), a member of my TextView held a reference to the parent text view. When I dereferenced my TextView, it stayed in memory.

Once I started keeping track of my references, things started making more sense.

## In conclusion...

I've been having more fun programming than watching Netflix. Trying to start and release open source software also feels more productive than binge-ing some series. However, I think I'm going to catch up on the latest Digimon series as a celebration for getting pretty far in terms of functionality.

I think [ThiefMD](https://thiefmd.com) is ready to handle some writing for something like [NaNoWriMo](https://nanowrimo.org). It doesn't have time goals or statistics yet, but it does have library management and an interface that promotes focus and writing.