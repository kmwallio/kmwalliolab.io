---
layout: post
title: Back to Xubuntu
date: 2018-06-10 08:30
categories: [life]
teaser: /images/xubuntu/teaser.jpg
comments: true
---

A while ago, I switched back to [Windows](https://www.microsoft.com/en-us/windows) so I could play video games...  While I enjoyed playing video games, one thing I missed was the integration of so many languages and the operating system.  So now, I'm back on [Xubuntu](https://xubuntu.org/).

This time, I put in a little more effort for finding apps I'll be using and creating a nice look and feel.  I'm documenting it, both to share, and in case I screw something up really bad.

<!-- more -->

## Why Xubuntu?

Most of it is just memory...  The first Linux Distro I used touted Xfce+Nautilus as a key reason to use it, and I've just stuck with Xfce+Nautilus.

I wish I could say it was for speed or better customization abilities or panel management, but it's just the default for me.  I use [Gnome Apps](https://wiki.gnome.org/Apps) and some intensive theme engines, so performance isn't much better.

## The Look and Feel

I'm using the [Vimix Light Beryl](https://github.com/vinceliuice/vimix-gtk-themes) theme.

![](/images/xubuntu/desktop.jpg)

For Xubuntu, I have 2 panels, one for a application menu, quick launcher, indicators, and clock at the top.  The second is an application menu, task bar, and shortcuts.

The icons are the beautiful [Paper Project](https://snwh.org/paper).

I was mostly inspired by [this reddit post](https://www.reddit.com/r/Ubuntu/comments/8m69oc/first_dive_back_into_ubuntu_in_3_years_heres_a/dzn96zf/), but wanted a brighter look and feel.

The wallpaper is Sunset by Layperson.  It used to be available on [Deviant Art](https://www.deviantart.com/), but the account no longer looks active.

## Markdown Editor

Originally, I was sold on [Ghostwriter](https://wereturtle.github.io/ghostwriter/).  It's feature rich, and does a lot more than the current markdown editor I'm using.  What I identified was I didn't need all of those features, and it was also missing that emotional feeling you get when using certain apps.

This is where I stumbled upon [elementary OS](https://elementary.io/) and read about [Quilter](https://medium.com/elementaryos/appcenter-spotlight-quilter-83e7f4b47336).  There's a lot of articles about [designing for emotion](https://medium.com/google-design/design-for-emotion-7ba0cf40e05b), and I feel like elementary OS and Quilter encompass that very well.  Although it's only distributed through the elementary OS AppCenter, it's [open source](https://github.com/lainsce/quilter) and easy to compile on Xubuntu.

```bash
$ sudo apt install valac libgranite-dev libgtkspell3-3-dev gtk+-3.0 gtksourceview-3.0 libwebkit2gtk-4.0-dev libmarkdown2-dev meson
$ git clone https://github.com/lainsce/quilter.git
$ cd quilter
$ meson build && cd build
$ meson configure -Dprefix=/usr
$ sudo ninja install
```

I've had a lot of fun hacking away at Quilter.  It's written in [Vala](https://wiki.gnome.org/Projects/Vala), which was easier to approach and I expected.

![](/images/xubuntu/shot-typewriter-scrolling.gif)

I've already added Dynamic Margins, Show File Name, and Typewriter Scrolling to it.  It's not as good as [Ulysses's Typewriter Mode](https://ulyssesapp.com/tutorials/typewriter-mode), but I'm hoping to get it pretty close with some additional things I've wanted.

## Terminal

I've started using [Tilix](https://gnunn1.github.io/tilix-web/).  I'm really all about the eyecandy at the moment, and Tilix provides both great eye candy and functionality.

## Git Client

I'm using [Gitg](https://wiki.gnome.org/Apps/Gitg/).  I haven't found anything like [Git Tower](https://www.git-tower.com/mac/) yet, but Gitg is pretty decent.

## Photos & Video

I haven't found a good photo and video editor yet.  For the most part, I'll be using my macBook and iPad for photography and syncing with [dropbox](https://www.dropbox.com).

## Going Forward

I'm really enjoying turning my computer into a welcoming environment.  I'm also enjoying the social coding aspect of it.  Finding projects and contributing back (selfishly) the features I want help me enjoy using my computer more.

Ever since working on a computer has become my 9-5, I've found my personal computer has to become more welcoming and more "homey" for me to sit down and write or program.

I'm really happy I've been able to contribute to a great project while building a great personal project environment.