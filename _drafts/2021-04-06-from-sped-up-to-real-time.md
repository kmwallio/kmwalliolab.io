---
layout: post
title: From Sped Up To Real Time
date: 2021-04-06 09:42:48
---
[Last Time](/blog/2021/03/31/speeding-kmwriter-up/) we added threading and caching to Grammar Checking to create an uninterrupted writing experience.

We were still doing grammar checking out of process. Every sentence required the computer to load `link-parser` into memory. The time adds up. We stress test [ThiefMD](https://thiefmd.com) against [Project Gutenberg](https://www.gutenberg.org/) novels. Some of them took a few minutes to scan. In process checking is significantly faster.

How do we call something in process from Vala? For that, we need a [Vala Binding (VAPI File)](https://wiki.gnome.org/Projects/Vala/Bindings). What if one doesn't exist? Today, we're making a [link-grammar](http://www.abisource.com/projects/link-grammar) Vala Binding.

<!-- more -->

## Writing a Vala Binding

Link Grammar does not use GObject Introspection. There's instructions on [Writing Bindings Manually](https://wiki.gnome.org/Projects/Vala/ManualBindings), but not everyone knows C. If you want to call Go, Rust, Haskell, or other languages from Vala, you'll need to write a VAPI that uses the C Application Binary Interface.