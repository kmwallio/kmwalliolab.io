---
layout: page
title: "Miles Wallio"
comments: false
sharing: false
footer: true
teaser: /images/about/teaser.jpg
---

Miles Wallio was a [computer science](http://cs.fit.edu) student at the [Florida Institute of Technology](http://fit.edu).  He earned his Master's in May 2013.  In May of 2011, he received his Bachelors' in computer science and mathematical sciences.

He currently works at [Microsoft](http://microsoft.com), and the opinions and views expressed on this site have no relation to that of his employer's.

You can stalk him on [Twitter](http://twitter.com/kmwallio) and [Instagram](http://instagram.com/kmwallio), or just [e-mail him](mailto:miles@wallio.net).

## Open Source Projects

* [ThiefMD](https://thiefmd.com) - [Markdown](https://daringfireball.net/projects/markdown) and [Fountain](https://fountain.io/) Editor for Linux. It's written in [Vala](https://wiki.gnome.org/Projects/Vala) and inspired by [Ulysses](https://ulysses.app/).
* [Theme Generator](https://github.com/ThiefMD/theme-generator) - Generate Ulysses Themes and [Gtk.SourceView](https://wiki.gnome.org/Projects/GtkSourceView) Style Schemes. Written in Vala.
* [WordPress](https://github.com/ThiefMD/wordpress-vala), [Ghost](https://github.com/ThiefMD/ghost-vala), and [WriteFreely](https://github.com/ThiefMD/writeas-vala) Client Libraries. Written in Vala.
* [libwritegood](https://writegood.thiefmd.com) - English Language Style Checker written in Vala. Based on [btford/write-good](https://github.com/btford/write-good).
* [BookMark'd](https://github.com/kmwallio/BookMarkd) - Link Saver and Searcher written in PHP.